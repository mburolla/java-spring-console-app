package com.example.demo;

import com.example.demo.repository.CatRepository;
import com.example.demo.repository.PersonRepository;
import com.example.demo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	protected CatRepository catRepository;

	@Autowired
	protected ProductRepository productRepository;

	@Autowired
	protected PersonRepository personRepository;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	//
	//  Spring Data JPA Playground
	//

	@Override
	public void run(String... args) {
		DBPopulator.populate(catRepository, productRepository, personRepository);

		var p = personRepository.findByFirstNameContaining("Gut").get(0);
		p.setLastName("Goven");
		personRepository.save(p);
		System.out.println(p);

	}
}
