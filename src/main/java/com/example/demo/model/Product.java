package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Product {

    //
    // Data members
    //

    @Id
    @GeneratedValue
    private int productId;
    private int departmentId;
    private String name;
    private float price;
    private String sku;

    //
    // Constructors
    //

    public Product() {
    }

    public Product(int departmentId, String name, float price, String sku) {
        this.departmentId = departmentId;
        this.name = name;
        this.price = price;
        this.sku = sku;
    }

    //
    // Accessors
    //

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    //
    // Overrides
    //

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product)) return false;
        Product product = (Product) o;
        return getProductId() == product.getProductId() && getDepartmentId() == product.getDepartmentId() && Float.compare(product.getPrice(), getPrice()) == 0 && getName().equals(product.getName()) && getSku().equals(product.getSku());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId(), getDepartmentId(), getName(), getPrice(), getSku());
    }
}
