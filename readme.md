# Java Spring Boot Console App 
A console app used to demonstrate data persistence via a console app.

# Application.Properties
Update `xxx` to correct values:
```
spring.datasource.driver-class-name = com.mysql.cj.jdbc.Driver
spring.datasource.url = jdbc:mysql://xxx:3306/xxx
spring.datasource.username = xxx
spring.datasource.password = xxx
spring.jpa.hibernate.ddl-auto = update
spring.jpa.show-sql = true
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect
spring.jpa.properties.format_sql = true
```

# Notes
- https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.sample-app.finders.strategies
